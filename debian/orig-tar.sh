#!/bin/sh -e
#
# Removes unwanted content from the upstream sources.
# Called by uscan with '--upstream-version' <version> <file>
#

VERSION=$2
TAR=../jnati_$VERSION.orig.tar.xz
DIR=jnati-$VERSION
TAG=$(echo "jnati-$VERSION" | sed -re's/~(alpha|beta|rc)/-\1-/')

svn export https://svn.code.sf.net/p/jnati/code/jnati/tags/${TAG}/ $DIR
XZ_OPT=--best tar -c -J -f $TAR --exclude '*.jar' --exclude '*.class' $DIR
rm -rf $DIR ../$TAG

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir && echo "moved $TAR to $origDir"
fi
